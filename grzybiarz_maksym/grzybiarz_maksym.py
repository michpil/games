import pygame as pg, sys, random, os
from time import sleep
from datetime import datetime


class Config:
    # Config
    root_folder = r'grzybiarz_maksym'
    pg.init()
    screen = pg.display.set_mode((1058, 770))
    max_fps = 10.0
    fps_clock = pg.time.Clock()
    fps_delta = 0.0

    # Sounds
    pg.mixer.music.load(os.path.join(root_folder, 'sounds', 'main_sound.mp3'))
    pg.mixer.music.play(0)

    take_ok_sound = pg.mixer.Sound(os.path.join(root_folder, 'sounds', 'take_ok1.mp3'))
    take_notok_sound = pg.mixer.Sound(os.path.join(root_folder, 'sounds', 'take_notok.mp3'))
    game_over_sound = pg.mixer.Sound(os.path.join(root_folder, 'sounds', 'game_over.mp3'))
    game_ok_sound = pg.mixer.Sound(os.path.join(root_folder, 'sounds', 'game_ok.mp3'))


class Max(Config):
    def __init__(self):
        self.step = 0
        self.points = 0
        self.koniec = ''
        self.font_points = (0, 0, 0)
        self.font_koniec = (0, 0, 0)
        self.start = datetime.now()
        self.stop = datetime.now()

        self.time_ = '0.00'
        self.font_time = (0, 0, 0)
        self.max_box_start = pg.Rect(10, 400, 100, 100)
        self.max_box = pg.Rect(10, 400, 100, 100)
        self.max_hat_box = pg.Rect(50, 330, 100, 150)
        self.max_look = 'right'

        self.max_stand_r = pg.image.load(os.path.join(self.root_folder, 'images', 'motion', 'r0a.png'))
        self.max_stand_l = pg.image.load(os.path.join(self.root_folder, 'images', 'motion', 'l0a.png'))
        self.max = self.max_stand_r
        self.screen.blit(self.max, self.max_box)

        self.max_hat = pg.image.load(os.path.join(self.root_folder, 'images', 'max_hat.png'))
        self.screen.blit(self.max_hat, self.max_hat_box)

        self.walk_right = [pg.image.load(os.path.join(self.root_folder, 'images', 'motion', 'r{}a.png'.format(numb))) for numb in range(1, 8)]
        self.walk_left = [pg.image.load(os.path.join(self.root_folder, 'images', 'motion', 'l{}a.png'.format(numb))) for numb in range(1, 8)]

    def show(self):
        self.screen.blit(self.max, self.max_box)
        self.screen.blit(self.max_hat, self.max_hat_box)

    def actions(self):
        keys = pg.key.get_pressed()
        is_pressed = False
        walk_pixel = 15
        if keys[pg.K_RIGHT]:
            is_pressed = True
            self.max_box.x += walk_pixel
            self.max_hat_box.x += walk_pixel
            self.step += 1
            if self.step == 7:
                self.step = 0

            self.max = self.walk_right[self.step]
            self.max_look = 'right'

        if keys[pg.K_LEFT]:
            is_pressed = True
            self.max_box.x -= walk_pixel
            self.max_hat_box.x -= walk_pixel
            self.step += 1
            if self.step == 7:
                self.step = 0

            self.max = self.walk_left[self.step]
            self.max_look = 'left'

        if keys[pg.K_DOWN]:
            is_pressed = True
            self.max_box.y += walk_pixel
            self.max_hat_box.y += walk_pixel
            self.step += 1
            if self.step == 7:
                self.step = 0

            if self.max_look == 'right':
                self.max = self.walk_right[self.step]
            elif self.max_look == 'left':
                self.max = self.walk_left[self.step]

        if keys[pg.K_UP]:
            is_pressed = True
            self.max_box.y -= walk_pixel
            self.max_hat_box.y -= walk_pixel
            if self.max_box.y < 130:
                self.max_box.y = 130
                self.max_hat_box.y = 60
            self.step += 1
            if self.step == 7:
                self.step = 0

            if self.max_look == 'right':
                self.max = self.walk_right[self.step]
            elif self.max_look == 'left':
                self.max = self.walk_left[self.step]

        if not is_pressed:
            if self.max_look == 'right':
                self.max = self.max_stand_r
            elif self.max_look == 'left':
                self.max = self.max_stand_l

    def collisions(self, collision_with, good_or_bad):
        move_off = 1000
        if self.max_hat_box.colliderect(collision_with) and good_or_bad == 'good':
            collision_with.x -= move_off
            collision_with.y += move_off
            self.points += 1
            self.take_ok_sound.play()

        if self.max_hat_box.colliderect(collision_with) and good_or_bad == 'bad':
            collision_with.x -= move_off
            collision_with.y += move_off
            self.points += 1
            self.font_points = (255, 0, 0)
            self.koniec = 'PRZEGRANA'
            self.font_koniec = (255, 0, 0)
            pg.mixer.music.stop()
            self.take_notok_sound.play()
            sleep(3)
            pg.mixer.music.stop()
            self.game_over_sound.play()

        if self.points == 7 and self.font_points == (0, 0, 0):
            self.font_points = (0, 255, 0)
            self.koniec = 'WYGRANA'
            self.font_koniec = (0, 255, 0)
            pg.mixer.music.stop()
            self.game_ok_sound.play()

        if self.max_box == self.max_box_start:
            self.start = datetime.now()
        elif self.max_box != self.max_box_start:
            self.start = self.start

        if self.koniec == '':
            self.stop = datetime.now()
        elif self.koniec in ['WYGRANA', 'PRZEGRANA']:
            self.stop = self.stop
        self.time_ = round((self.stop - self.start).total_seconds(), 2)


class Mushroom(Config):
    def __init__(self, picture):
        self.picture = picture
        self.limits_x = int(random.uniform(100, 950))
        self.limits_y = int(random.uniform(350, 650))
        self.size = 50

        self.mushroom_box = pg.Rect(self.limits_x, self.limits_y, self.size, self.size)
        self.name = pg.image.load(self.picture)

    def show(self):
        self.screen.blit(self.name, self.mushroom_box)


class GameElements(Config):
    def show(self, points, font_points, koniec, font_koniec, time_, font_time):
        # background
        bg = pg.image.load(os.path.join(self.root_folder, 'images', 'bg2.jpg'))
        self.screen.blit(bg, (0, 0))

        # punkty
        font = pg.font.SysFont('Arial', 100)
        text = font.render(str(points), True, font_points, (255, 255, 255))
        self.screen.blit(text, (10, 10))

        # timer
        font = pg.font.SysFont('Arial', 100)
        text = font.render(str(time_), True, font_time, (255, 255, 255))
        self.screen.blit(text, (840, 10))

        # koniec
        font = pg.font.SysFont('Arial', 100)
        text = font.render(koniec, True, font_koniec, (255, 255, 255))
        self.screen.blit(text, (300, 300))


class Game(Config):
    def __init__(self):
        # Config
        pg.init()
        pg.display.set_caption('Grzybiarz Maksym')
        game_icon = pg.image.load(os.path.join(self.root_folder, 'images', 'icon1.png'))
        pg.display.set_icon(game_icon)

        maks = Max()
        kurka = Mushroom(os.path.join(self.root_folder, 'images', 'kurka1.png'))
        borowik = Mushroom(os.path.join(self.root_folder, 'images', 'prawdziwek1.png'))
        muchomor = Mushroom(os.path.join(self.root_folder, 'images', 'muchomor1.png'))
        podgrzybek = Mushroom(os.path.join(self.root_folder, 'images', 'podgrzybek1.png'))
        kozak = Mushroom(os.path.join(self.root_folder, 'images', 'kozak.png'))
        czerwony = Mushroom(os.path.join(self.root_folder, 'images', 'czerwony.png'))
        kania = Mushroom(os.path.join(self.root_folder, 'images', 'kania.png'))
        rydz = Mushroom(os.path.join(self.root_folder, 'images', 'rydz.png'))

        game_elements = GameElements()

        while True:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    sys.exit(0)

            game_elements.show(maks.points, maks.font_points, maks.koniec, maks.font_koniec, maks.time_, maks.font_time)

            for element in [kurka, borowik, muchomor, podgrzybek, kozak, czerwony, kania, rydz]:
                element.show()

            maks.show()

            pg.display.flip()

            self.fps_delta += self.fps_clock.tick() / 1000.0
            while self.fps_delta > 1 / self.max_fps:
                maks.actions()

                for element in [(kurka.mushroom_box, 'good'),
                                (borowik.mushroom_box, 'good'),
                                (muchomor.mushroom_box, 'bad'),
                                (podgrzybek.mushroom_box, 'good'),
                                (kozak.mushroom_box, 'good'),
                                (czerwony.mushroom_box, 'good'),
                                (kania.mushroom_box, 'good'),
                                (rydz.mushroom_box, 'good')]:
                    maks.collisions(element[0], element[1])

                self.fps_delta -= 1 / self.max_fps


if __name__ == "__main__":
    Game()
